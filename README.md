For Installation check offical site.
/usr/local/bin -->sed this location to axcess terraform from all tools and all users.
terraform -v
========================================================
terraform init 
terraform plan --var-file terraform.tfvars --->play is used to dry run
terraform plan
Difference between 7 & 6 lines ?if your not using tfvars files then tf variable file is excuted.
terraform apply --var-file terraform.tfvars 
==============================================================
tfstate file ?after apply then state file is created work like source code version
terraform apply --var-file terraform.tfvars --auto-approve
terraform destroy --var-file terraform.tfvars --auto-approve
terraform plan --var-file terraform.tfvars -var aws_access_key=xxxxxxxxxxxxxx -var aws_secret_key= yyyyyyyyy
terraform output files
==================================================================
Multiple tfstate:Better option is create different folder for each project or use workspaces
terraform workspace new test
terraform workspace select test
terraform modules 
terraform get
======================================================================
tsstate file in backend:you ned to configure aws_access_key
Role based axcess
Create a role and attach to terraform server
terraform backend with dynamodb locking:Not possible to edit files by multiple users
======================================================================
terraform refresh
==============================================
terraform import
terraform import --var-file aws_instance.example i-abcd1234
==================================================
terraform validate :validate terraform files
================================================
terraform taint              Manually mark a resource for recreation
terraform untaint            Manually unmark a resource as tainted
====================================================
terraform data resourses:used for existing data
==================================================
depends on :it will create first
======================
functions:
count
list
element(list,index)
map
lookup(map ,index)
length
conditions:(condition?truevalue:falsevalue)
===================================================
Common commands:
    apply              Builds or changes infrastructure
    console            Interactive console for Terraform interpolations
    destroy            Destroy Terraform-managed infrastructure
    env                Workspace management
    fmt                Rewrites config files to canonical format
    get                Download and install modules for the configuration
    graph              Create a visual graph of Terraform resources
    import             Import existing infrastructure into Terraform
    init               Initialize a Terraform working directory
    output             Read an output from a state file
    plan               Generate and show an execution plan
    providers          Prints a tree of the providers used in the configuration
    push               Upload this Terraform module to Atlas to run
    refresh            Update local state file against real resources
    show               Inspect Terraform state or plan
    taint              Manually mark a resource for recreation
    untaint            Manually unmark a resource as tainted
    validate           Validates the Terraform files
    version            Prints the Terraform version
    workspace          Workspace management

===============================================================
All other commands:
    debug              Debug output management (experimental)
    force-unlock       Manually unlock the terraform state
    state              Advanced state managemen


