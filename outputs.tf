output "vpc_security_group_ids=" {
  description = "List of associated security groups of instances, if running in non-default VPC"
  value       = ["${aws_vpc.public_vpc.default_security_group_id}"]
}

output "security_groups=" {
  description = "List of associated security groups of instances"
  value       = ["${aws_security_group.TestSG.id}"]
}

output "testserverid=" {
  description = "List of associated security groups of instances, if running in non-default VPC"
  value       = "${aws_instance.testserver.id}"
}